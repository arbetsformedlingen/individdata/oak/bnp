// server/index.js
const express = require("express");
const PORT = process.env.PORT || 3001;
const app = express();
var cors = require('cors')
const corstOptions = {
  origin:'http://localhost:3000', 
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200
}

app.use(cors(corstOptions)) // Use this after the variable declaration
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});


app.get("/consent", (req, res) => {
    res.json({ message: "your personal data will now be consented by the digital wallet" });
  });
